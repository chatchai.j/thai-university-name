<?php
function localeArray($arr)
{
    $returner = array();
    if (is_array($arr)) {
        foreach ($arr as $str) {
            if (mb_ereg_match('\p{Thai}', $str) > 0) {
                $returner['th_TH'] = $str;
            } else {
                $returner['en_US'] = $str;
            }
        }
    } else {
        $lang = (mb_ereg_match('\p{Thai}', $arr)) ? 'th_TH':'en_US';
        $returner[$lang] = $arr;
    }
    return $returner;
}

function searchKey($key, $arr)
{
    if (is_array($arr)) {
        foreach ($arr as $cats) {
            foreach ($cats as $subcat) {
                foreach ($subcat as $uni) {
                    if (isset($uni['acronym']['en_US']) && ($uni['acronym']['en_US'] === $key)) {
                        return $uni;
                    }
                }
            }
        }
    }

    return null;
}
