<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR .'list_en.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR .'list_th.php';


function mergeUniverse($type = 'json')
{
    $th = thCrawler('array');
    $en = enCrawler('array');
    $merge = array();

    foreach ($th as $keycat => $cats) {
        if (!isset($merge[$keycat])) {
            $merge[$keycat] = array();
        }

        foreach ($cats as $keysub => $subcat) {
            if (!isset($merge[$keycat][$keysub])) {
                $merge[$keycat][$keysub] = array();
            }

            foreach ($subcat as $keyuni => $uni) {
                if (!isset($merge[$keycat][$keysub][$keyuni])) {
                    $merge[$keycat][$keysub][$keyuni] = array();
                }

                if (isset($uni['acronym']['en_US'])) {
                    $key = $uni['acronym']['en_US'];
                }
                $result = searchKey($key, $en);
                if ($result) {
                    $name = (array_merge($uni['name'], $result['name']));
                    $location = (array_merge($uni['location'], $result['location']));
                } else {
                    $name = $uni['name'];
                    $location = $uni['location'];
                }

                $merge[$keycat][$keysub][$keyuni] = array(
                 'name' => $name,
                 'acronym' => $uni['acronym'],
                 'founded' => array('B.E.' => $uni['founded'],'A.D.' => $result['founded']),
                 'location' => $location
                );
            }
        }
    }

    return ($type === 'json') ? json_encode($merge, JSON_PRETTY_PRINT|JSON_NUMERIC_CHECK|JSON_UNESCAPED_UNICODE):$merge;
}
