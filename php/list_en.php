<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR .'functions.php';

function enCrawler($type = 'json')
{
    $uniCats = array(
    'Public universities and colleges',
    'Rajabhat Universities system',
    'Rajamangala Universities of Technology system',
    'Military and Police Academies',
    'Non-Ministry of Education-Affiliated Colleges and Institutes',
    'Intergovernmental institute'
    );
    $subCats = array(
    'Government universities' => array(0,2,4,6),
    'Autonomous universities' => array(0,2,4,6),
    'Rajabhat Universities system'=> array(2,4,6,8),
    'Rajamangala Universities of Technology system'=> array(2,4,6,8),
    'Military and Police Academies'=> array(0,2,4,6),
    'Non-Ministry of Education-Affiliated Colleges and Institutes'=> array(0,2,4,6),
    'Universities'=> array(2,4,6,8),
    'Institutes'=> array(2,4,6,8),
    'Colleges'=> array(2,4,6,8),
    'Intergovernmental institute'=> array(2,4,6,8)
    );

    $url = 'https://en.wikipedia.org/wiki/List_of_universities_and_colleges_in_Thailand';

    $keyCat = function ($title) {
        return preg_replace('/\[.*.\]+$/u', '', trim($title));
    };

    $doc = new DOMDocument();
    $doc->loadHTMLFile($url);

    $finder = new DomXPath($doc);
    $classname="mw-parser-output";

    $university = array();


    $elements = $finder->query("//div[@class='mw-parser-output']");

    $useTable = true;

    if (!is_null($elements)) {
        foreach ($elements as $element) {
            $nodes = $element->childNodes;
            foreach ($nodes as $node) {
                if (($node->nodeName === "h2") && (in_array($keyCat($node->nodeValue), $uniCats))) {
                    $catKey = $keyCat($node->nodeValue);
                    if (!isset($university[$catKey])) {
                        $university[$catKey] = array();
                    }

                    if ($useTable) {
                        $subKey = 'Government universities';
                        if (!isset($university[$catKey][$subKey])) {
                            $university[$catKey][$subKey] = array();
                        }
                        $useTable = false;
                    }
                    if ($catKey === 'Intergovernmental institute') {
                        $subKey = 'Intergovernmental institute';
                        if (!isset($university[$catKey][$subKey])) {
                            $university[$catKey][$subKey] = array();
                        }
                    }
                }
                if (($node->nodeName === "h3") && (array_key_exists($keyCat($node->nodeValue), $subCats))) {
                    $subKey = $keyCat($node->nodeValue);
                    if (!isset($university[$catKey][$subKey])) {
                        $university[$catKey][$subKey] = array();
                    }
                }


                if ($node->nodeName === "table") {
                    $table = $node->childNodes;
                    $tidx = 0;
                    $uniLine = array();
                    foreach ($table as $tEl) {
                        if ($tidx >= 1) {
                            $tr = $tEl->childNodes;
                            $tdidx = 0;

                            foreach ($tr as $td) {
                                switch ($tdidx) {
          case $subCats[$subKey][0]:
            $name = localeArray($keyCat($td->nodeValue));
            break;

          case $subCats[$subKey][1]:
            $acronym = localeArray(explode('/', $keyCat($td->nodeValue)));
            break;

          case $subCats[$subKey][2]:
            $founded = $keyCat($td->nodeValue);
            break;

          case $subCats[$subKey][3]:
            $location = localeArray($keyCat($td->nodeValue));
            break;

         } //switch
                                $tdidx++;
                            } // foreach
                            $uniLine[] = array(
            'name' => $name,
            'acronym' => $acronym,
            'founded' => $founded,
            'location' => $location,
        );
                        } //else
                        $tidx++;
                    } // foreach
                    if (isset($subKey)) {
                        $university[$catKey][$subKey] = array_merge($university[$catKey][$subKey], $uniLine);
                    }
                } // if
            }  //foreach
        } // foreach
    } // if

return ($type === 'json') ? json_encode($university, JSON_PRETTY_PRINT|JSON_NUMERIC_CHECK|JSON_UNESCAPED_UNICODE):$university;
}
